/* *********************************************

UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS
FACULTAD DE INGENIERIA DE SISTEMAS E INFORMATICA
ESCUELA:
ALUMNO:
PROFESOR:

FECHA:

CURSO:

********************************************* */

#include <iostream>
#include <stdlib.h>

using namespace std;

int cont=0; // Un contador para los elementos de la lista

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo

struct tNodo{
	int dato;
	tLista siguiente;
};

// PROTOTIPOS DE LAS FUNCIONES

tLista crearNodo(int dato);
int sizeLista(tLista lista);
//tLista ultimoNodo(tLista lista);
int menu();
void mostrar(tLista lista);
void insertarInicio(tLista &lista,int dato);
void insertarFinal(tLista &lista,int dato);
void insertarPosicion(tLista &lista,int dato,int pos);
void eliminarPrimero(tLista &lista);
void eliminarUltimo(tLista &lista);
void eliminarPosicion(tLista &lista,int pos);

void ordenamientoBurbuja(tLista &lista);
//void ordenamientoInsercion(tLista &lista);
void insertarNodo(tLista &listaOrdenada,int valor);
void ordenamientoSeleccion(tLista &lista);
void ordenamientoIntercambio(tLista &lista);
void insertarInsercion(tLista &,int );
void ordenamientoInsercion(tLista &);
void insertarPosicion2(tLista &, int, int );

// FUNCION PRINCIPAL

int main(){

    tLista lista = NULL; // Lista vacia

    int dato,pos;
    int opc;

    do {
        opc = menu();
        switch(opc){
            case 1:
                    cout<<"Ingrese dato: "; cin>>dato;
                   	insertarInicio(lista,dato);
					mostrar(lista);
                    break;
            case 2:

                	cout<<"Ingrese dato: ";cin>>dato;
                	insertarFinal(lista,dato);
					mostrar(lista);
                    break;
            case 3:
					cout<<"Ingrese dato: ";cin>>dato;
                	cout<<"Ingrese posicion: ";cin>>pos;
                	insertarPosicion(lista,dato,pos);
                	mostrar(lista);
                	break;

            case 4: 
					ordenamientoBurbuja(lista);
                    mostrar(lista);
                    break;
            case 5: ordenamientoSeleccion(lista);
            		mostrar(lista);
                    break;
            case 6:	ordenamientoIntercambio(lista);
            		mostrar(lista);
                    break;
            case 7:
            		ordenamientoInsercion(lista);
            		mostrar(lista);
                    break;
            case 8:
                    eliminarPrimero(lista);
                    mostrar(lista);
					break;
            case 9:
                	eliminarUltimo(lista);
                	mostrar(lista);
					break;
       	 	case 10:
					cout<<"Ingrese la posicion:";cin>>pos;
                    eliminarPosicion(lista,pos);
                    mostrar(lista);
					break;
        	case 11:
                    mostrar(lista);
					break;
           	default:
           			cout<<"Ingrese una opcion valida"<<endl;
        }

    }while(opc!=0);

    system("pause");
	return(0);
}

// IMPLEMENTACION DE LAS FUNCIONES

tLista crearNodo(int dato){
	tLista nuevo = new tNodo;
	nuevo->dato = dato;
	nuevo->siguiente = NULL;

	return nuevo;
}

void mostrar(tLista lista){
	tLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
	//cout<<"N� elementos: "<<cont<<endl<<endl;
}

void insertarInicio(tLista &lista,int dato){
	tLista nuevo = crearNodo(dato);

	if(lista == NULL){
		lista = nuevo;
	}else{
	    nuevo->siguiente = lista;
	    lista = nuevo;
	}
	//cont++;
}

void insertarFinal(tLista &lista,int dato){
	tLista nuevo = crearNodo(dato);

	if(lista == NULL){
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;
	}
    //cont++;
}

void insertarPosicion(tLista &lista,int dato,int pos){

	if(lista == NULL){
		cout<<"Lista vacia"<<endl;
	}else{
		if(0<pos && pos<=(sizeLista(lista)+1)){
			if(pos==1){
				insertarInicio(lista,dato);
			}else if(pos == (sizeLista(lista)+1)){
				insertarFinal(lista,dato);
			}else{
				tLista nuevo = crearNodo(dato);
				tLista p = lista;
				int contador = 1;
				while(contador < (pos-1) && p->siguiente != NULL){
					p = p->siguiente;
					contador++;
				}
				nuevo->siguiente = p->siguiente;
				p->siguiente = nuevo;
			}
			//cont++;
		}
		else{
			cout<<"Posicion invalida"<<endl;
		}

	}

}

void eliminarPrimero(tLista &lista){

	if(lista == NULL){
		cout<<"Lista vacia";
	}else{
		tLista temporal = lista;
        lista = lista->siguiente;
        delete temporal;
        //cont--;
	}

}

void eliminarUltimo(tLista &lista){

	if(lista==NULL){
		cout<<"Lista vacia";
	}else{
		if(lista->siguiente == NULL){
			delete lista;
			lista = NULL;
		}else{
			tLista p = lista;
			while(p->siguiente->siguiente != NULL){
				p = p->siguiente;
			}
			tLista temporal = p->siguiente;
		    p->siguiente = NULL;
		    delete temporal;
		}
		//cont--;
	}
}

void eliminarPosicion(tLista &lista,int pos){

	if(lista == NULL){
		cout<<"Lista vacia"<<endl;
	}else{
		if(0<pos && pos<=sizeLista(lista)){
			if(pos==1){
				eliminarPrimero(lista);
			}else if(pos == sizeLista(lista)){
				eliminarUltimo(lista);
			}else{
				tLista p = lista;
				int contador = 0;
				while(contador < (pos-1) && p->siguiente != NULL){
					p = p->siguiente;
					contador++;
				}
				tLista temporal = p->siguiente;
				p->siguiente = p->siguiente->siguiente;
				delete temporal;
			}
			//cont--;
		}
		else{
			cout<<"Posicion invalida"<<endl;
		}

	}
}

void ordenamientoBurbuja(tLista &lista){
	
	int num;
	tLista p = lista;
	bool intercambio = true;
	
	do{
		if(p->siguiente == NULL){
			p = lista;
			intercambio = false;
		}
		if(p->dato > p->siguiente->dato){
			num = p->dato;
			p->dato = p->siguiente->dato;
			p->siguiente->dato = num;
			intercambio = true;
		}
		p = p->siguiente;
	}while(intercambio || p->siguiente!=NULL);
		
	/*
    tLista ultimo = ultimoNodo(lista);
    bool interruptor = true;

    while(lista!=NULL && interruptor){
        tLista p = lista;
        interruptor = false;
        while(p!=ultimo){
            if(p->dato = p->siguiente->dato){
                int aux = p->dato;
                p->dato = p->siguiente->dato;
                p->siguiente->dato = aux;
                interruptor = true;
            }
            ultimo = p;
            p = p->siguiente;
        }

    }*/
}

/*void ordenamientoInsercion(tLista &lista){
	
	tLista p = lista;
	tLista aux =NULL;
	tLista listaOrdenada = crearNodo(p->dato);
	p=p->siguiente;
	
	while(p!=NULL){
		
		if(dato>p->dato){
			p=p->siguiente;
			while(p!=NULL && dato>p->dato){
				aux=p;
				p=p->siguiente;
			}
			
			if(dato>p->dato){
				
			}else{
				insertarFinal(p,dato);
			}
		}else{
			insertarInicio(p,dato);
		}
	}
	

}*/

void insertarNodo(tLista &listaOrdenada,int dato){
	tLista p = listaOrdenada;
	int pos=1;
	while((dato>p->dato) && (p->siguiente!=NULL)){
		pos++;
		p = p->siguiente;	
	}
	
	insertarPosicion(listaOrdenada,dato,pos);
			
}

void ordenamientoIntercambio(tLista &lista){
	tLista p = lista;
	tLista inicio = p->siguiente;
	
	while(p->siguiente!=NULL){
		
		while(inicio!=NULL){		
			if((p->dato) > (inicio->dato)){
				int aux;
				aux = inicio->dato;
				inicio->dato = p->dato;
				p->dato = aux;
			}
			inicio = inicio->siguiente;
		}
		p = p->siguiente;
		inicio = p->siguiente;	
	}
	
	
	
}


void ordenamientoSeleccion(tLista &lista){

int minimo =999999999;
int pre =0;
tLista apuntadorauxiliar = lista;
tLista apuntadorauxiliar2 = lista;
tLista apuntadorauxiliar3 = lista;

	while (apuntadorauxiliar->siguiente !=NULL){
		
		apuntadorauxiliar2 = apuntadorauxiliar->siguiente;
		apuntadorauxiliar3 = apuntadorauxiliar;
	
		while (apuntadorauxiliar2!=NULL){
		
			if (minimo > apuntadorauxiliar2->dato)
			minimo = apuntadorauxiliar2->dato;
			apuntadorauxiliar2 = apuntadorauxiliar2->siguiente;
		}
	
		while (apuntadorauxiliar3 -> dato != minimo){
			apuntadorauxiliar3 = apuntadorauxiliar3->siguiente;
		}
	
		if (minimo < apuntadorauxiliar->dato){
			pre = apuntadorauxiliar -> dato;
			apuntadorauxiliar -> dato = minimo;
			apuntadorauxiliar3->dato = pre;
		}
	
		apuntadorauxiliar = apuntadorauxiliar->siguiente;
		minimo = 50000000;
		
	}

}


int sizeLista(tLista lista){
	tLista p = lista;
	int size = 0;
	while(p!=NULL){
		size++;
		p=p->siguiente;
	}
	
	return size;
	
}

void insertarInsercion(tLista &lista,int dato){
	tLista nueva=lista;
	int n=1;
	bool insertar=false;
	while(nueva->siguiente!=NULL && insertar==false){
		if(dato<nueva->dato){
			insertarPosicion2(lista,n,dato);
			insertar=true;
		}
		n++;
		nueva=nueva->siguiente;
	}
	if(dato>nueva->dato){
		insertarFinal(nueva,dato);
	}
}

void ordenamientoInsercion(tLista &lista){
	tLista p=lista, nueva=NULL, q;
	q=(new struct tNodo);
	q->dato=p->dato;
	q->siguiente=NULL;
	if(nueva==NULL){
		nueva=q;
	}
	p=p->siguiente;
	
	while(p!=NULL){
		if(p->dato<nueva->dato){
			insertarInicio(nueva,p->dato);
		}
		else{
				insertarInsercion(nueva,p->dato);
		}
		p=p->siguiente;
	}
	lista=nueva;
}

void insertarPosicion2(tLista &lista, int pos, int valor){
	tLista p, nuevo;
	nuevo=(new struct tNodo);
	nuevo->dato=valor;
	int n=1;
	p=lista;
	while(n!=pos-1 && p->siguiente!=NULL){
		n++;
		p=p->siguiente;
	}
	nuevo->siguiente=p->siguiente;
	p->siguiente=nuevo;
}

int menu() {

    int opc;

	cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1]  Adicionar al inicio"<<endl;
	cout<<"[2]  Adicionar al final"<<endl;
	cout<<"[3]  Adicionar por posicion"<<endl;
	cout<<"[4]  Ordenamiento burbuja"<<endl;
	cout<<"[5]  Ordenamiento por seleccion"<<endl;
	cout<<"[6]  Ordenamiento por intercambio"<<endl;
	cout<<"[7]  Ordenamiento por insercion"<<endl;
	cout<<"[8]  Eliminar primero"<<endl;
	cout<<"[9]  Eliminar ultimo"<<endl;
	cout<<"[10] Eliminar posicion"<<endl;
	cout<<"[11] Mostrar"<<endl;
	cout<<"[0] Salir"<<endl;
	cout<<"Ingrese opcion: "; cin>>opc;

    return  opc;
}


